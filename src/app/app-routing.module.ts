import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    // 1st Route
    { path: '', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) }
    // 2nd Route
    ,{ path: '**', loadChildren: () => import('./features/page-not-found/page-not-found.module').then(m => m.PageNotFoundModule) }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {
                enableTracing: true, // <-- debugging purposes only
            }
        )
    ],
    exports: [ RouterModule ],
    providers: []
})

export class AppRoutingModule { }